# -*- coding: utf-8 -*-
"""
Created on Thu May 18 22:05:39 2017

@author: 106380
"""


import matplotlib.pyplot as plt

IP_SERVER = '192.168.43.208:5000'
# TOP: http://192.168.43.208:5000/leaderboard
NAME_OF_GROUP = 'mjmm'
NAME_OF_MODEL = 'MIMODELO'

######################################################

import numpy as np
import requests
import json
import pandas as pd

X_train = np.load(open('X_train.npy', 'rb'))
Y_train = np.load(open('Y_train.npy', 'rb'))
X_test = np.load(open('X_test.npy', 'rb'))

num_examples = len(Y_train)
# X_train.shape
ceros = 0
unos = 0
errors = 0
for i in range(0, num_examples):
    if Y_train[i] == 0:
        ceros += 1
    elif Y_train[i] == 1:
        unos += 1
    else:
        errors += 1

print("Percent of ones: ", unos / num_examples)
print("Percent of zeros: ", ceros / num_examples)

if errors == 0:
    print("No exist errors")

XtrainFrame = pd.DataFrame(X_train)

correlacion = XtrainFrame.corr(method='pearson')

matrixCorrelacion = correlacion.as_matrix()

print(matrixCorrelacion)

# Comprobamos que existe una correlación entre las variables 3 y 4  y las variables 5 y 6.
# También existe una correlación en menor grado pero igualmente se considera correlación a las variables el grupo 3/4 y 5/6 (con valor 0.96)
# También existe una correlación entre la variable 2 y 7 de 0.98
# Eliminamos las siguientes características: 4,5,6,7.


X_train = np.delete(X_train, [3, 4, 5, 6], 1)
X_test = np.delete(X_test, [3, 4, 5, 6], 1)

PORCENTAJE_TEST = 0.15

XtestPrevio = X_train[int(np.floor((1 - PORCENTAJE_TEST)) * len(X_train)):, :]
ytestPrevio = Y_train[int(np.floor((1 - PORCENTAJE_TEST)) * len(Y_train)):]

Xtrainingval = X_train[0:int(np.floor((1 - PORCENTAJE_TEST) * len(X_train))), :]
ytrainingval = Y_train[0:int(np.floor((1 - PORCENTAJE_TEST) * len(X_train)))]

########################################################

from sklearn.ensemble import RandomForestClassifier

classifier = RandomForestClassifier(n_estimators=125, max_features=1, class_weight='balanced')
print("Entrenando el modelo")
classifier.fit(Xtrainingval, ytrainingval)

print("Predicción")
Y_predtest = classifier.predict(XtestPrevio)



lenTest = len(Y_predtest)

print("TP")
TP = 0
for i in range(lenTest):
    if Y_predtest[i] == 1:
        if ytestPrevio[i]== 1:
            TP += 1
print(TP)

print("TN")
TN = 0
for i in range(lenTest):
    if Y_predtest[i] == 0:
        if ytestPrevio[i] == 0:
            TN += 1
print(TN)

print("FP")
FP = 0
for i in range(lenTest):
    if Y_predtest[i] == 1:
        if ytestPrevio[i] == 0:
            FP += 1
print(FP)

print("FN")
FN = 0
for i in range(lenTest):
    if Y_predtest[i] == 1:
        if ytestPrevio[i] == 0:
            FN += 1
print(FN)

print("ACCURACY")
print((TP+TN)/lenTest)

print("True positive rate")
print(TP/(sum(ytestPrevio == 1)))


# print ("Predición de 1: " + str(sum(Y_predtest != (ytestPrevio == 1))/float(sum(ytestPrevio==1))))
# print ("Predición de 0: " + str(sum(Y_predtest != (ytestPrevio == 0))/float(sum(ytestPrevio==0))))

######################################################

# data = list(Y_pred) + [NAME_OF_GROUP] + [NAME_OF_MODEL]
# data_json = json.dumps(data)
# headers = {'Content-Type': 'application/json'}
# response = requests.post('http://' + IP_SERVER + '/score', data=data_json, headers=headers)
# from tornado import escape
# scores = escape.json_decode(response.content)

# print("Your Test Accuracy score is: " + str(round(scores["accuracy"],3)))
# print("Your Test F1 score is: " + str(round(scores["f1"],3)))
# print("Your Test ROC score is: " + str(round(scores["roc"],3)))
# print("Your Test Precision score is: " + str(round(scores["precision"],3)))
# print("Your Test Recall score is: " + str(round(scores["recall"],3)))
